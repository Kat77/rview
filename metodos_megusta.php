<?php
include ("conexion.php");

switch($_GET['funcion']){
	case 'agregarMeGusta':
		agregarMeGusta();
		break;
	case 'quitarMeGusta':
		quitarMeGusta();
		break;
	case 'obtenerMeGustaPorResena':
		obtenerMeGustaPorResena();
		break;
}

function agregarMeGusta(){
	$consulta="INSERT INTO megusta (idResena, idUsuario) VALUES (".$_GET['idResena'].",".$_GET['idUsuario'].")";
	mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
	mysqli_close($GLOBALS['conexion']);
}

function quitarMeGusta(){
	$consulta="DELETE FROM megusta WHERE idUsuario=".$_GET['idUsuario']." AND idResena=".$_GET['idResena'];
	mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
	mysqli_close($GLOBALS['conexion']);
}

function obtenerMeGustaPorResena(){
	$consulta="SELECT COUNT(*) FROM megusta WHERE idResena=".$_GET['idResena'];
	$datos=mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
	$fila=mysqli_fetch_array($datos);

	$numMeGusta = array('numMeGusta' => $fila['COUNT(*)']);
	$meGustaJsonEncoded = json_encode($numMeGusta);
	mysqli_close($GLOBALS['conexion']);

	return $meGustaJsonEncoded;
}

?>