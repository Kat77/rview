<?php
include ("conexion.php");

switch($_GET['funcion']){
	case 'obtenerTodasResenas':
		obtenerTodasResenas();
		break;
	case 'obtenerMisResenas':
		obtenerMisResenas();
		break;
	case 'obtenerResenasPorCategoria':
		obtenerResenasPorCategoria();
		break;
	case 'agregarResena':
		agregarResena();
		break;
	case 'editarResena':
		editarResena();
		break;
	case 'eliminarResena':
		eliminarResena();
		break;
	case 'obtenerComentariosPorResena':
		obtenerComentariosPorResena();
		break;
}

function agregarResena(){
	$consulta="INSERT INTO resenas (idCategoria, idUsuario, contenido, estrellas, titulo) VALUES (".$_GET['idCategoria'].", ".$_GET['idUsuario'].", '".$_GET['contenido']."','".$_GET['estrellas']."','".$_GET['titulo']."')";
	mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
	mysqli_close($GLOBALS['conexion']);
}

function editarResena(){
	$consulta="UPDATE resenas SET idCategoria=".$_GET['idCategoria'].", idUsuario=".$_GET['idUsuario'].", contenido='".$_GET['contenido']."', estrellas='".$_GET['estrellas']."', titulo='".$_GET['titulo']."' WHERE idResena=".$_GET['idResena'];
	mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
	mysqli_close($GLOBALS['conexion']);
}

function eliminarResena(){
	$consulta="DELETE FROM resenas WHERE idResena=".$_GET['idResena'];
	mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
	mysqli_close($GLOBALS['conexion']);
}

function obtenerTodasResenas(){
	$arrayResenas = array();
	$consulta="SELECT * FROM resenas";
	$datos=mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
	while ($fila=mysqli_fetch_array($datos)){
		$resena = array(
			'idResena' => $fila['idResena'],
		    'contenido' => $fila['contenido'],
		    'estrellas' => $fila['estrellas'],
		    'titulo' => $fila['titulo'],
		);
		array_push($arrayResenas, $resena);
	}
	$arrayResenasJsonEncoded = json_encode($arrayResenas);
	mysqli_close($GLOBALS['conexion']);
	
	return $arrayResenasJsonEncoded;
}

function obtenerMisResenas(){
	$arrayResenas = array();
	$consulta="SELECT * FROM resenas WHERE idUsuario=".$_GET['idUsuario'];
	$datos=mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
	while ($fila=mysqli_fetch_array($datos)){
		$resena = array(
			'idResena' => $fila['idResena'],
		    'contenido' => $fila['contenido'],
		    'estrellas' => $fila['estrellas'],
		    'titulo' => $fila['titulo'],
		);
		array_push($arrayResenas, $resena);
	}
	$arrayResenasJsonEncoded = json_encode($arrayResenas);
	mysqli_close($GLOBALS['conexion']);
	
	return $arrayResenasJsonEncoded;
}


function obtenerResenasPorCategoria(){
	$arrayResenas = array();
	$consulta="SELECT * FROM resenas WHERE idCategoria=".$_GET['idCategoria'];
	$datos=mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
	while ($fila=mysqli_fetch_array($datos)){
		$resena = array(
			'idResena' => $fila['idResena'],
		    'contenido' => $fila['contenido'],
		    'estrellas' => $fila['estrellas'],
		    'titulo' => $fila['titulo'],
		);
		array_push($arrayResenas, $resena);
	}
	$arrayResenasJsonEncoded = json_encode($arrayResenas);
	mysqli_close($GLOBALS['conexion']);
	echo $arrayResenasJsonEncoded;
	return $arrayResenasJsonEncoded;
}

function obtenerComentariosPorResena(){
	$arrayComentarios = array();
	$consulta="SELECT * FROM comentarios WHERE idResena=".$_GET['idResena'];
	$datos=mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
	while ($fila=mysqli_fetch_array($datos)){
		$resena = array(
			'idComentario' => $fila['idComentario'],
		    'descripcion' => $fila['descripcion'],
		    'fecha' => $fila['fecha']
		);
		array_push($arrayComentarios, $resena);
	}
	$arrayComentariosJsonEncoded = json_encode($arrayComentarios);
	mysqli_close($GLOBALS['conexion']);
	echo $arrayComentariosJsonEncoded;
	return $arrayComentariosJsonEncoded;
}

?>