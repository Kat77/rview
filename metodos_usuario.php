<?php
include ("conexion.php");

switch($_GET['funcion']){
	case 'registrar':
		registrar();
		break;
	case 'login':
		login();
		break;
	case 'obtenerSeguidos':
		obtenerSeguidos();
		break;
	case 'obtenerSeguidores':
		obtenerSeguidores();
		break;
	case 'verPerfil':
		verPerfil();
		break;
	case 'editarUsuario':
		editarUsuario();
		break;
	case 'eliminarUsuario':
		eliminarUsuario();
		break;
}

function registrar(){
	$consulta="INSERT INTO usuarios (correo, username, contrasena) VALUES ('".$_GET['correo']."','".$_GET['username']."','".$_GET['contrasena']."')";
	mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
	mysqli_close($GLOBALS['conexion']);
}

function login(){
	$consulta="SELECT * FROM usuarios WHERE correo='".$_GET['correo']."' and contrasena='".$_GET['contrasena']."'";
	$datos=mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
	if($fila=mysqli_fetch_array($datos)){
		mysqli_close($GLOBALS['conexion']);
		return true;
	}
	mysqli_close($GLOBALS['conexion']);

	return false;
}

function editarUsuario(){
	$consulta="UPDATE usuarios SET username='".$_GET['username']."', contrasena='".$_GET['contrasena']."', correo='".$_GET['correo']."', sobremi='".$_GET['sobremi']."' WHERE idUsuario=".$_GET['idUsuario'];
	mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
	mysqli_close($GLOBALS['conexion']);
}

function verPerfil(){
	$arrayUsuario = array();
	$consulta="SELECT * FROM usuarios WHERE idUsuario=".$_GET['idUsuario'];
	$datos=mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
	while ($fila=mysqli_fetch_array($datos)){
		$usuario = array(
		    'idUsuario' => $fila['idUsuario'],
		    'username' => $fila['username'],
		    'contrasena' => $fila['contrasena'],
		    'correo' => $fila['correo'],
		    'sobremi' => $fila['sobremi']
		);
		array_push($arrayUsuario, $usuario);
	}
	$arrayUsuarioJsonEncoded = json_encode($arrayUsuario);
	mysqli_close($GLOBALS['conexion']);
	
	return $arrayUsuarioJsonEncoded;
}

function eliminarUsuario(){
	$consulta="DELETE FROM usuarios WHERE idUsuario=".$_GET['idUsuario'];
	mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
	mysqli_close($GLOBALS['conexion']);
}

function obtenerSeguidos(){
	$arraySeguidos = array();
	$consulta="SELECT s.idUsuarioSeguido, u.username, u.correo FROM usuarios u INNER JOIN seguidores s ON s.idSeguidor=".$_GET['idSeguidor']." and u.idUsuario=s.idSeguidor";
	$datos=mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
	while ($fila=mysqli_fetch_array($datos)){
		$seguido = array(
		    'idUsuarioSeguido' => $fila['idUsuarioSeguido'],
		    'username' => $fila['username'],
		    'correo' => $fila['correo'],
		);
		array_push($arraySeguidos, $seguido);
	}
	$arraySeguidosJsonEncoded = json_encode($arraySeguidos);
	mysqli_close($GLOBALS['conexion']);

	return $arraySeguidosJsonEncoded;
}

function obtenerSeguidores(){
	$arraySeguidores = array();
	$consulta="SELECT s.idSeguidor, u.username, u.correo FROM usuarios u INNER JOIN seguidores s ON s.idUsuarioSeguido=".$_GET['idUsuarioSeguido']." and u.idUsuario=s.idUsuarioSeguido";
	$datos=mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
	while ($fila=mysqli_fetch_array($datos)){
		$seguido = array(
		    'idSeguidor' => $fila['idSeguidor'],
		    'username' => $fila['username'],
		    'correo' => $fila['correo'],
		);
		array_push($arraySeguidores, $seguido);
	}
	$arraySeguidosJsonEncoded = json_encode($arraySeguidores);
	mysqli_close($GLOBALS['conexion']);

	return $arraySeguidosJsonEncoded;
}

?>