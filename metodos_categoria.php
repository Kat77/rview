<?php
include ("conexion.php");

switch($_GET['funcion']){
	case 'obtenerTodasCategorias':
		obtenerTodasCategorias();
		break;
}

function obtenerTodasCategorias(){
	$arrayCategorias = array();
	$consulta="SELECT * FROM categoria";
	$datos=mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
	while ($fila=mysqli_fetch_array($datos)){
		$categoria = array(
		    'idCategoria' => $fila['idCategoria'],
		    'nombre' => $fila['nombre']
		);
		array_push($arrayCategorias, $categoria);
	}
	$arrayCategoriasJsonEncoded = json_encode($arrayCategorias);
	mysqli_close($GLOBALS['conexion']);

	return $arrayCategoriasJsonEncoded;
}

?>